// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIafsdIcerndOchdIworkdImdImrkimdITest3dICMSSW_8_1_0dIsrcdIHiggsAnalysisdICombinedLimitdIsrcdIHZZ2L2QRooPdfs_cc_ACLiC_dict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "."
#include "/afs/cern.ch/work/m/mrkim/Test3/CMSSW_8_1_0/src/HiggsAnalysis/CombinedLimit/src/HZZ2L2QRooPdfs.cc"

// Header files passed via #pragma extra_include

   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *ROOT_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("ROOT", 0 /*version*/, "Rtypes.h", 144,
                     ::ROOT::Internal::DefineBehavior((void*)0,(void*)0),
                     &ROOT_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *ROOT_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }

namespace {
  void TriggerDictionaryInitialization_HZZ2L2QRooPdfs_cc_ACLiC_dict_Impl() {
    static const char* headers[] = {
"HZZ2L2QRooPdfs.cc",
0
    };
    static const char* includePaths[] = {
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/include",
"/afs/cern.ch/work/m/mrkim/Test3/CMSSW_8_1_0/src",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/cms/cmssw/CMSSW_8_1_0/src",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/cms/coral/CORAL_2_3_21-giojec12/include/LCG",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/mctester/1.25.0a-giojec14/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/qt/4.8.7/include/QtDesigner",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/herwigpp/7.0.2-giojec4/include/Herwig",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/tauolapp/1.1.5-giojec7/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/charybdis/1.003-giojec7/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/sherpa/2.2.1-giojec5/include/SHERPA-MC",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/rivet/2.5.2/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/qt/4.8.7/include/QtOpenGL",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/qt/4.8.7/include/QtGui",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/thepeg/2.0.2-giojec3/include/ThePEG",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/pythia8/219-giojec5/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/herwig/6.521-giojec7/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/qt/4.8.7/include/Qt3Support",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/classlib/3.1.3/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/lhapdf/6.1.6-giojec7/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/cgal/4.2-giojec6/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/tkonlinesw/4.1.0-1-giojec7/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/starlight/r193-giojec7/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/qt/4.8.7/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/qt/4.8.7/include/Qt",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/qt/4.8.7/include/QtCore",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/qt/4.8.7/include/QtXml",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/mcdb/1.0.3/interface",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/libungif/4.1.4/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/libtiff/4.0.3/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/libpng/1.6.16/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/geant4/10.02.p02-giojec5/include/Geant4",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/frontier_client/2.8.20/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/pcre/8.37/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/boost/1.57.0-giojec4/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/xz/5.2.1/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/xrootd/4.4.1/include/xrootd/private",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/cms/vdt/v0.3.2-giojec2/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/valgrind/3.11.0/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/utm/r47119-xsd330-patch-giojec2/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/toprex/4.23/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/tbb/44_20151115oss/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/tauola/27.121.5/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/sigcpp/2.6.2/include/sigc++-2.0",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/sqlite/3.12.2-giojec2/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/protobuf/2.6.1/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/pacparser/1.3.5/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/oracle/12.1.0.2.0/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/meschach/1.2.pCMS1/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/libhepml/0.2.1/interface",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/ktjet/1.06-giojec7/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/jimmy/4.2-giojec7/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/jemalloc/4.2.1/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/madgraph5amcatnlo/2.4.3-giojec4",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/heppdt/3.03.00-giojec/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/hector/1.3.4_patch1-giojec14/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/gsl/2.2.1/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/libjpeg-turbo/1.3.1/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/giflib/4.2.3/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/xerces-c/3.1.3/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/gdbm/1.10/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/freetype/2.5.3/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/fftw3/3.3.2/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/fftjet/1.5.0/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/fastjet/3.1.0/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/expat/2.1.0/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/hepmc/2.06.07/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/dpm/1.8.0.1/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/dcap/2.47.8/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/libxml2/2.9.1/include/libxml2",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/curl/7.47.1/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/cppunit/1.12.1/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/clhep/2.3.1.1-giojec4/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/openssl/1.0.2d/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/pythia6/426/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/photos/215.5/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/zlib/1.2.8/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/libuuid/2.22.2/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/castor/2.1.13.9/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/castor/2.1.13.9/include/shift",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/cascade/2.2.04-giojec7/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/bz2lib/1.0.6/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/python/2.7.11-giojec4/include/python2.7",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/gcc/5.3.0/include/c++/5.3.0",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/gcc/5.3.0/include/c++/5.3.0/x86_64-pc-linux-gnu",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/gcc/5.3.0/include/c++/5.3.0/backward",
"/usr/local/include",
"/usr/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/etc",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/gcc/5.3.0/bin/../lib/gcc/x86_64-pc-linux-gnu/5.3.0/../../../../include/c++/5.3.0",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/gcc/5.3.0/bin/../lib/gcc/x86_64-pc-linux-gnu/5.3.0/../../../../include/c++/5.3.0/x86_64-pc-linux-gnu",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/gcc/5.3.0/bin/../lib/gcc/x86_64-pc-linux-gnu/5.3.0/../../../../include/c++/5.3.0/backward",
"/build/cmsbld/auto-builds/CMSSW_8_1_0-slc7_amd64_gcc530/build/CMSSW_8_1_0-build/BUILD/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/build/interpreter/cling/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/etc/cling",
"/build/cmsbld/auto-builds/CMSSW_8_1_0-slc7_amd64_gcc530/build/CMSSW_8_1_0-build/BUILD/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/root-6.06.08",
"/build/cmsbld/auto-builds/CMSSW_8_1_0-slc7_amd64_gcc530/build/CMSSW_8_1_0-build/BUILD/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/build/include",
"/build/cmsbld/auto-builds/CMSSW_8_1_0-slc7_amd64_gcc530/build/CMSSW_8_1_0-build/BUILD/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/root-6.06.08/graf3d/g3d/inc",
"/build/cmsbld/auto-builds/CMSSW_8_1_0-slc7_amd64_gcc530/build/CMSSW_8_1_0-build/BUILD/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/root-6.06.08/gui/gui/inc",
"/build/cmsbld/auto-builds/CMSSW_8_1_0-slc7_amd64_gcc530/build/CMSSW_8_1_0-build/BUILD/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/root-6.06.08/io/io/inc",
"/build/cmsbld/auto-builds/CMSSW_8_1_0-slc7_amd64_gcc530/build/CMSSW_8_1_0-build/BUILD/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/root-6.06.08/core/base/../textinput/src",
"/build/cmsbld/auto-builds/CMSSW_8_1_0-slc7_amd64_gcc530/build/CMSSW_8_1_0-build/BUILD/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/build/core/base/",
"/build/cmsbld/auto-builds/CMSSW_8_1_0-slc7_amd64_gcc530/build/CMSSW_8_1_0-build/BUILD/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/build/core/rint/",
"/build/cmsbld/auto-builds/CMSSW_8_1_0-slc7_amd64_gcc530/build/CMSSW_8_1_0-build/BUILD/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/build/core/thread/",
"/build/cmsbld/auto-builds/CMSSW_8_1_0-slc7_amd64_gcc530/build/CMSSW_8_1_0-build/BUILD/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/build/io/io/",
"/build/cmsbld/auto-builds/CMSSW_8_1_0-slc7_amd64_gcc530/build/CMSSW_8_1_0-build/BUILD/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/root-6.06.08/hist/hist/inc",
"/build/cmsbld/auto-builds/CMSSW_8_1_0-slc7_amd64_gcc530/build/CMSSW_8_1_0-build/BUILD/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/build/math/mathcore/",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/etc",
"/afs/cern.ch/work/m/mrkim/Test3/CMSSW_8_1_0/src",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/cms/cmssw/CMSSW_8_1_0/src",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/cms/coral/CORAL_2_3_21-giojec12/include/LCG",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/mctester/1.25.0a-giojec14/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/qt/4.8.7/include/QtDesigner",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/herwigpp/7.0.2-giojec4/include/Herwig",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/tauolapp/1.1.5-giojec7/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/charybdis/1.003-giojec7/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/sherpa/2.2.1-giojec5/include/SHERPA-MC",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/rivet/2.5.2/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/qt/4.8.7/include/QtOpenGL",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/qt/4.8.7/include/QtGui",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/thepeg/2.0.2-giojec3/include/ThePEG",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/pythia8/219-giojec5/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/herwig/6.521-giojec7/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/qt/4.8.7/include/Qt3Support",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/classlib/3.1.3/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/lhapdf/6.1.6-giojec7/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/cgal/4.2-giojec6/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/tkonlinesw/4.1.0-1-giojec7/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/starlight/r193-giojec7/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/qt/4.8.7/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/qt/4.8.7/include/Qt",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/qt/4.8.7/include/QtCore",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/qt/4.8.7/include/QtXml",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/mcdb/1.0.3/interface",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/libungif/4.1.4/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/libtiff/4.0.3/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/libpng/1.6.16/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/geant4/10.02.p02-giojec5/include/Geant4",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/frontier_client/2.8.20/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/pcre/8.37/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/boost/1.57.0-giojec4/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/xz/5.2.1/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/xrootd/4.4.1/include/xrootd/private",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/cms/vdt/v0.3.2-giojec2/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/valgrind/3.11.0/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/utm/r47119-xsd330-patch-giojec2/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/toprex/4.23/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/tbb/44_20151115oss/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/tauola/27.121.5/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/sigcpp/2.6.2/include/sigc++-2.0",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/sqlite/3.12.2-giojec2/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/protobuf/2.6.1/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/pacparser/1.3.5/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/oracle/12.1.0.2.0/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/meschach/1.2.pCMS1/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/libhepml/0.2.1/interface",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/ktjet/1.06-giojec7/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/jimmy/4.2-giojec7/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/jemalloc/4.2.1/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/madgraph5amcatnlo/2.4.3-giojec4",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/heppdt/3.03.00-giojec/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/hector/1.3.4_patch1-giojec14/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/gsl/2.2.1/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/libjpeg-turbo/1.3.1/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/giflib/4.2.3/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/xerces-c/3.1.3/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/gdbm/1.10/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/freetype/2.5.3/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/fftw3/3.3.2/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/fftjet/1.5.0/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/fastjet/3.1.0/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/expat/2.1.0/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/hepmc/2.06.07/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/dpm/1.8.0.1/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/dcap/2.47.8/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/libxml2/2.9.1/include/libxml2",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/curl/7.47.1/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/cppunit/1.12.1/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/clhep/2.3.1.1-giojec4/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/openssl/1.0.2d/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/pythia6/426/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/photos/215.5/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/zlib/1.2.8/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/libuuid/2.22.2/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/castor/2.1.13.9/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/castor/2.1.13.9/include/shift",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/cascade/2.2.04-giojec7/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/bz2lib/1.0.6/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/python/2.7.11-giojec4/include/python2.7",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/gcc/5.3.0/include/c++/5.3.0",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/gcc/5.3.0/include/c++/5.3.0/x86_64-pc-linux-gnu",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/gcc/5.3.0/include/c++/5.3.0/backward",
"/usr/local/include",
"/usr/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/gcc/5.3.0/bin/../lib/gcc/x86_64-pc-linux-gnu/5.3.0/../../../../include/c++/5.3.0",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/gcc/5.3.0/bin/../lib/gcc/x86_64-pc-linux-gnu/5.3.0/../../../../include/c++/5.3.0/x86_64-pc-linux-gnu",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/external/gcc/5.3.0/bin/../lib/gcc/x86_64-pc-linux-gnu/5.3.0/../../../../include/c++/5.3.0/backward",
"/afs/cern.ch/work/m/mrkim/Test3/CMSSW_8_1_0/src/HiggsAnalysis/CombinedLimit/src/",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc530/lcg/root/6.06.08-giojec3/include",
"/afs/cern.ch/work/m/mrkim/Test3/CMSSW_8_1_0/src/HiggsAnalysis/CombinedLimit/src/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "HZZ2L2QRooPdfs_cc_ACLiC_dict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "HZZ2L2QRooPdfs_cc_ACLiC_dict dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef __ACLIC__
  #define __ACLIC__ 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "HZZ2L2QRooPdfs.cc"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"ROOT::GenerateInitInstance", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("HZZ2L2QRooPdfs_cc_ACLiC_dict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_HZZ2L2QRooPdfs_cc_ACLiC_dict_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_HZZ2L2QRooPdfs_cc_ACLiC_dict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_HZZ2L2QRooPdfs_cc_ACLiC_dict() {
  TriggerDictionaryInitialization_HZZ2L2QRooPdfs_cc_ACLiC_dict_Impl();
}
