//********************************************************
//* RUNII H->ZZ->2l2q combine limit:CJLST+xBF 
//* Author:Mi Ran Kim
//* Sungkyunkwan University.South Korea
//* 2nd.Jan.2023
//* run with: root -l -b llqq_Run2Comb.C++
//********************************************************
#include <iomanip>//ste::setprecision
#include <fstream>
#include <string>
#include <vector>
#include <math.h>
#include <cmath>
#include <cstdlib>
#include "TCanvas.h"
#include "TColor.h"
#include "TFile.h"
#include "TFrame.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TH1.h"
#include "TH2.h"
#include "TLegend.h"
#include "TLegendEntry.h"
#include "TLine.h"
#include "TMath.h"
#include "TPaletteAxis.h"
#include "TROOT.h"
#include "TSpline.h"
#include "TStyle.h"
#include "TSystem.h"
#include "TTree.h"
#include "TChain.h"
#include <vector>
#include "TLorentzVector.h"
#include "stdlib.h"

//#include "../interface/HZZ4L_RooHighmass_1D.h"//From Axeli, not working
//#include "../interface/SplinePdf.h"//exist, SplinePdf.h from Axel
//#include "../interface/VerticalInterpPdf.h"//exist
//#include "../interface/ProcessNormalization.h"//exist
//#include "../interface/HZZ2L2QRooPdfs.h"//exits
//#include "../interface/HZZ4L_RooHighmass.h"

#include "HZZ4L_RooHighmass_1D.cc"//From Axeli, not working
#include "HZZ4L_RooHighmass.cc"//From Axel
//#include "HZZ2L2QRooPdfs.cc+"//exits
//#include "ProcessNormalization.cc+"//exist
//#include "VerticalInterpPdf.cc+"//exist
#include "SplinePdf.cc"//exist, SplinePdf.h from Axel

using namespace std;
using namespace RooFit;


TString chanName [2]={"eeqq","mumuqq"};
TString jetName [2]={"Merged","Resolved"};//Merged(?jetType=0),Resolved(?jetType=1)
TString jetNameLower [2]={"merged","resolved"};
TString cateName [3]={"vbf-tagged","b-tagged","untagged"};
TString sampleName[2]={"ggH","VBF"};
TString dataName[2]={"ggH","qqH"};
TString workName[2]={"1D","2D"};
TString mzz_name[2]= {"ZZMass","ZZMass"};
float btagU[2]={0.2,0.05};//?,Not Clear
float jesU[2]={0.1,0.05};//?,Not Clear

//h125 sample
TString hName[2]={
//	"/eos/cms/store/group/phys_higgs/cmshzz4l/cjlst/RunIIUL/MC2016post/ggH124/ZZ4lAnalysis.root",
//	"/eos/cms/store/group/phys_higgs/cmshzz4l/cjlst/RunIIUL/MC2016post/VBFH124/ZZ4lAnalysis.root",
	"/eos/cms/store/group/phys_higgs/cmshzz4l/cjlst/RunIILegacy/200205_CutBased/AC16/ggH0PH_M125/ZZ4lAnalysis.root",
	"/eos/cms/store/group/phys_higgs/cmshzz4l/cjlst/RunIILegacy/200205_CutBased/AC16/VBFH0PH_M125/ZZ4lAnalysis.root",
};

//Why need?
double lowEdge_reco[2]={500,500};
//double lowEdge[2]={500,5000};//for test
double lowEdge[2]={450,450};
float hxsec[2]={0.855*3.04,0.248*1.4568*1.15};//? 3.04E-02:h125-quak-loop induced: 2e2q,2mu2q, off shell
float bkgxsec[2]={3.19,0.642};//ggzz,qqzz
//Resolution parametrization

double dcbPara_resolved_2nd[6][11]={
1000,1500,-1.10999, 0.00749822, -4.5463e-06, 4.04181, -0.00280538, 6.05505e-07, 3.98471, -0.00272925, 5.80127e-07, 
1000,1500,0.0665556, 0.00364624, -2.39996e-06, 3.3315, -0.00288365, 8.64985e-07, 2.01928, -0.00113402, 2.81776e-07, 
1000,1500,2.6701, 1.00153, -1.70739e-05, 44.0866, 0.918697, 2.43426e-05, -32.3866, 1.02066, -9.64553e-06, 
1000,1500,15.9333, -0.0311407, 1.6586e-05, 3.3495, -0.0059731, 4.00217e-06, -6.32149, 0.00692155, -2.96044e-07, 
1000,1500,1.62105, 0.00202981, 5.77829e-07, -2.19538, 0.00966267, -3.2386e-06, 5.18966, -0.00018405, 4.36387e-08, 
1000,1500,-2.2513, 0.0303664, -2.88022e-07, -4.51134, 0.0348865, -2.54806e-06, 32.5499, -0.0145285, 1.39236e-05
};

double dcbPara_merged_2nd[6][11]={
1000,1500,1.79404, -0.0011454, 6.50693e-07, 1.01854, 0.0004056, -1.24807e-07, 1.22566, 0.00012944, -3.27534e-08, 
1000,1500,-1.40284, 0.00996887, -5.86499e-06, 5.91995, -0.00467671, 1.4578e-06, 2.76244, -0.000466697, 5.44607e-08, 
1000,1500,-26.1949, 1.07972, -4.16358e-05, 24.2941, 0.978742, 8.8532e-06, -4.35604, 1.01694, -3.8802e-06, 
1000,1500,10.7366, -0.00214211, -1.24418e-06, 14.652, -0.00997291, 2.67122e-06, 10.5603, -0.00451731, 8.52683e-07, 
1000,1500,5.55773, -0.0146569, 1.11044e-05, -11.6884, 0.0198354, -6.14177e-06, 0.79069, 0.00319657, -5.95509e-07, 
1000,1500,4.59191, 0.0348467, -5.74807e-06, 10.704, 0.0226225, 3.64019e-07, 6.08797, 0.0287772, -1.68755e-06
};

//Start 
void dosomething(int chan=0,int jetType=0, int cate=0, int sample=0,int is2D=1,double mass_d=450, double width_d=46.8){
 
  RooWorkspace w("w");
  gStyle->SetOptStat(0);
  double sigfull, bkgfull,hfull,lumi,h_bkgfull;
  lumi = 35.8;//2016:35.9fb-1, 2017:41.5fb-1, 2018:59.7fb-1
  sigfull = 2*0.0673*0.699*lumi;//?BR(Z->ll):0.0673, 0.699:?
  hfull = (hxsec[sample]*lumi*0.699)/0.0336*2;//3.04E-2 LHC YR 
  bkgfull = (bkgxsec[sample]*lumi*0.699)/0.0336*2;//3.04E-2 LHC YR
  cout<<"sigfull="<<sigfull<<endl;//3.36826
  cout<<"hfull="<<hfull<<endl;//=3871.6
  cout<<"bkgfull="<<bkgfull<<endl;//4751.62
 

  double dcbPara_2nd[6][11];//?for Resolution parametrization, di Meung
//  RooConstVar dcbPara_2nd[6][11];//Not working due to double can not be the input for  RooArgList
  double parzx[5]={0.};
  if(jetType==1){
    for (int i=0;i<6;i++){for(int j=0;j<11;j++){dcbPara_2nd[i][j]= dcbPara_resolved_2nd[i][j];}}
  }
  else{
    for (int i=0;i<6;i++){for(int j=0;j<11;j++){dcbPara_2nd[i][j]= dcbPara_merged_2nd[i][j];}}
  }
 
  //range of gen and reco
  const double low=lowEdge[jetType];
  const double high=3600;
  const int nbins=(high-low)/2;
  cout<<"nbins="<<nbins<<endl;//1575 bins
  const double low_reco=lowEdge_reco[jetType];
  const double high_reco=3500;
  const int nbins_reco=(high_reco-low_reco); 
  cout<<"nbins_reco="<<nbins_reco<<endl;//3000 bins
 
  
 //definiton gen mass and reco mass
  RooRealVar* mzz = new RooRealVar("genmass","M_{ZZ} (GeV)",400,low,high);
  RooRealVar* mean = new RooRealVar("mean","mean",450,100,3000);
  RooRealVar* sigma= new RooRealVar("sigma","sigma",50,0.,2000);
  RooRealVar *r= new RooRealVar("r","",1,0,1000);
  RooRealVar *vbfrac= new RooRealVar("fvbf","",0,0,1.);

  RooRealVar* dbkg= new RooRealVar("Dbkg_0plus","Dbkg_{kin} ",0.5,0.,1.);
  RooRealVar* mreco= new RooRealVar(mzz_name[jetType],"M_{ZZ} (GeV)",400,low_reco,high_reco);

  RooFormulaVar* signorm;
  if(sample==0)
    signorm =new RooFormulaVar("signorm_"+sampleName[sample],"@0*(1-@1)",RooArgSet(*r,*vbfrac));
  else{
    vbfrac->setVal(1);
    signorm =new RooFormulaVar("signorm_"+sampleName[sample],"@0*@1",RooArgSet(*r,*vbfrac));
  }

  RooPlot* frame_dbkg= dbkg->frame(Title("")) ;
  RooPlot* frame= mreco->frame(Title("Z mass")) ;
  RooPlot* frame_mzz= mzz->frame(Title("Z mass")) ;

//Signla xsec

double ggHMass[71]={500,550,600,650,700,750,800,850,900,950,1000,1050,1100,1150,1200,1250,1300,1350,1400,1450,1500,1550,1600,1650,1700,1750,1800,1850,1900,1950,2000,2050,2100,2150,2200,2250,2300,2350,2400,2450,2500,2550,2600,2650,2700,2750,2800,2850,2900,2950,3000,3050,3100,3150,3200,3250,3300,3350,3400,3450,3500,3550,3600,3650,3700,3750,3800,3850,3900,3950,4000};
double ggHxsec[71]={1.709,1.297,1.001,7.834E-01,6.206E-01,4.969E-01,4.015E-01,3.271E-01,2.685E-01,2.219E-01,1.845E-01,1.542E-01,1.295E-01,1.093E-01,9.260E-02,7.880E-02,6.730E-02,5.760E-02,4.950E-02,4.270E-02,3.690E-02,3.190E-02,2.770E-02,2.410E-02,2.100E-02,1.838E-02,1.609E-02,1.411E-02,1.239E-02,1.090E-02,9.600E-03,8.470E-03,7.480E-03,6.620E-03,5.860E-03,5.190E-03,4.610E-03,4.090E-03,3.640E-03,3.240E-03,2.890E-03,2.570E-03,2.300E-03,2.050E-03,1.830E-03,1.640E-03,1.460E-03,1.310E-03,1.170E-03,1.050E-03,9.400E-04,8.470E-04,7.600E-04,6.820E-04,6.130E-04,5.510E-04,4.950E-04,4.450E-04,4.000E-04,3.600E-04,3.240E-04,2.920E-04,2.630E-04,2.370E-04,2.130E-04,1.920E-04,1.730E-04,1.560E-04,1.410E-04,1.270E-04,1.140E-04};
double vbfHMass[51]={500,550,600,650,700,750,800,850,900,950,1000,1050,1100,1150,1200,1250,1300,1350,1400,1450,1500,1550,1600,1650,1700,1750,1800,1850,1900,1950,2000,2050,2100,2150,2200,2250,2300,2350,2400,2450,2500,2550,2600,2650,2700,2750,2800,2850,2900,2950,3000};
double vbfHxsec[51]={4.872E-01,3.975E-01,3.274E-01,2.719E-01,2.275E-01,1.915E-01,1.622E-01,1.380E-01,1.180E-01,1.013E-01,8.732E-02,7.551E-02,6.550E-02,5.698E-02,4.970E-02,4.345E-02,3.807E-02,3.343E-02,2.942E-02,2.592E-02,2.288E-02,2.023E-02,1.791E-02,1.588E-02,1.410E-02,1.253E-02,1.115E-02,9.926E-03,8.849E-03,7.896E-03,7.052E-03,6.303E-03,5.638E-03,5.046E-03,4.520E-03,4.051E-03,3.633E-03,3.259E-03,2.925E-03,2.627E-03,2.360E-03,1.908E-03,1.717E-03,1.545E-03,1.391E-03,1.252E-03,1.128E-03,1.016E-03,9.158E-04,8.253E-04};

TSpline3 *lo = new TSpline3("lo",ggHMass,ggHxsec,71);//xsec of ggH
double testXsec;
testXsec=lo->Eval(mass_d);
cout<<"ggH_"<<mass_d<<"'s xsec="<<testXsec<<endl;

mean->setVal(mass_d);//0x7ffe31df9be8
sigma->setVal(width_d);//0x7ffe31df9be0
//  cout<<"mean="<<&mean<<endl;//
//  cout<<"sigma="<<&sigma<<endl;//
SplinePdf *pdf;//Analitical shape
pdf = new SplinePdf("pdf","",*mzz,*mean,*sigma,*lo);//Analitical shape of the signal 
//lo->plotOn(frame_mzz);//Not working
//pdf->plotOn(frame_mzz);//OK
//frame_mzz->Draw();

// "lo" draw test:Not working
double w1=600; double hh1=600;
//auto *C_test_2e2q=new TCanvas("C_test_2e2q","C_test_2e2q",w1,hh1);
//C_test_2e2q->SetWindowSize(w1+(w1-C_test_2e2q->GetWw()),hh1+(hh1-C_test_2e2q->GetWh()));
//C_test_2e2q->cd();
//lo->Draw("lcpsame");

//?input 2l2q samples?
//TChain *da= new TChain("selectedTree");
//da->Add(Form("/afs/cern.ch/work/m/mr/test/myWorkingArea/CMSSW_8_0_6/src/ZZMatrixElement/MELA/test/2l2q/%smH%.0f_gen.root",sampleName[sample].Data(),mass_d));//ggHmH450_gen.root

//?input for what kfactori:for background(Not use maybe), Is it different use from gghKfac_input_spline.root"
//TFile *fkfactor = new TFile("Kfactor_Collected_ggHZZ_2l2l_NNLO_NNPDF_NarrowWidth_13TeV.root");
//TGraph *ggZZ_kf = (TGraph*)fkfactor->Get("kfactor_Nominal");
 
//h125 input, Standard Modle(S.M) h125 input, start without this,
  mean->setVal(125.);
  sigma->setVal(0.00407);//?why this value
//  TChain *t125 = new TChain ("SelectedTree");//Tree name not correct
  TChain *t125 = new TChain ("ZZTree/candTree");
  t125->Add(hName[sample]);
  double hall = t125->GetEntries("ZZMass!=0");
  double hsel = t125->GetEntries(Form("ZZMass>%f&&ZZMass<%f",low,high));//!zero, low=450, high=3600, 450<ZZMass<3600
  double hnum = hsel/hall*hfull;//? for what
//  cout<<"hall="<<hall<<endl;//118345
//  cout<<"hsel="<<hsel<<endl;//zero, !?:if this is smH125,any distribution exist, therfore hsel=0.
//  cout<<"hnum="<<hnum<<endl;//zero,!due to hsel=0,


 //?for what pdf125
  TH1F *pdf125_hist=new TH1F("pdf125_hist","",nbins,low,high);
  double bwid = (high-low)/double(nbins);//2
  double bwid2 = (high-low)/nbins;//2
  cout<<"bwid="<<bwid<<endl;//2
  cout<<"bwid2="<<bwid2<<endl;//2
  for(int k=0;k<nbins;k++){
    double x = pdf125_hist->GetBinCenter(k+1);
    cout<<"x="<<x<<endl;//451,453,455...3599
    mzz->setVal(x);//RooRealVar* mzz value setting as x
//    cout<<"mzz="<<mzz<<endl;//! 0x7fffd428f5f8
    double bc_125= pdf->getVal(*mzz)*hnum*bwid;//!Zero due to hnum=0, Due to pdf,can not be teste
    if(x==125){
      bc_125=0;
      for(int j=-50;j<50;j++){
        mzz->setVal(x+j*0.2*0.00407);//?for what?
	bc_125+=pdf->getVal(*mzz)*hnum*0.2*0.00407;//?for what?
      }//end of for(int j=-50;j<50;j++)    
    }//end of if(x==125)
  pdf125_hist->SetBinContent(k+1,bc_125);//?for what	
  }//End of for(int k=0;k<nbins;k++)
//  pdf125_hist->Draw("histo");//? for what, can not draw


  

//?Back ground samples input, for ggH,VBFH input,
//?Maybe not necessary
//TChin *ggzz=new TChain("SelectedTree");
//  TChain *ggzz=new TChain("ZZTree/candTree");
//  ggzz->Add("/eos/cms/store/group/phys_higgs/cmshzz4l/cjlst/RunIILegacy/200205_CutBased/AC16/ggH0PH_M125/ZZ4lAnalysis.root");//Not correct file,ggzz need of ggH
//  ggzz->Add("/eos/cms/store/group/phys_higgs/cmshzz4l/cjlst/RunIILegacy/200205_CutBased/AC16/ggH0PH_M125/ZZ4lAnalysis.root");//Not correct file

//  TChain *vbfbkg= new TChain("ZZTree/candTree");
//  TChain *vbfbkg_failed= new TChain("ZZTree/candTree_failed");
//  vbfbkg->Add("/eos/cms/store/group/phys_higgs/cmshzz4l/cjlst/RunIILegacy/200205_CutBased/AC16/VBFH0PH_M125/ZZ4lAnalysis.root");//Not correct file, Maybe qqzz
//  vbfbkg_failed->Add("/eos/cms/store/group/phys_higgs/cmshzz4l/cjlst/RunIILegacy/200205_CutBased/AC16/VBFH0PH_M125/ZZ4lAnalysis.root");//Not correct file
//  double bkgall = ggzz->GetEntries("");//Not zero
//  double bkgsel = ggzz->GetEntries(Form("ZZMass>%f&&ZZMass<%f",low,high));//!zero, low=450, high=3600, 450<ZZMass<3600
//  if(smaple){
//      bkgall = vbfbkg->GetEntries("")+vbfbkg_failed->GetEntries("");
//      bkgsel = vbfbkg->GetEntries(Form("GenHMass>%f&&GenHMass<%f",low,high))+vbfbkg_failed->GetEntries(Form("GenHMass>%f&&GenHMass<%f",low,high));
//  }//end of if(smaple)
//  double bkgnum = bkgsel/bkgall*bkgfull;//zero duto bkgsel=0
//  cout<<"bkgall="<<bkgall<<endl;//118345
//  cout<<"bkgsel="<<bkgsel<<endl;//zero
//  cout<<"bkgfull="<<bkgfull<<endl;//Not zero,4751.62
//  cout<<"bkgnum="<<bkgnum<<endl;//zero due to bkgsel
  
  //? for what
//  TH1F *genm= new TH1F ("genm","",nbins,low,high);//?Empty histo
//  genm->Scale(bkgnum/genm->Integral());

//

/*
//?Method1, Resolutio parametrizatio Method1: p0 stand for?
TString formu_2nd=" (@0<@1)*(@3+@0*@4+@0*@0*@5 ) + ( @0>=@1 && @0<@2)*(@6+@0*@7+@0*@0*@8) + (@0>=@2)*(@9+@0*@10+@0*@0*@11)";

RooArgList formuList_a1;
RooArgList formuList_a2;
RooArgList formuList_mean;
RooArgList formuList_n1;
RooArgList formuList_n2;
RooArgList formuList_sigma;

formuList_a1.add(*mzz);
formuList_a2.add(*mzz);
formuList_mean.add(*mzz);
formuList_n1.add(*mzz);
formuList_n2.add(*mzz);
formuList_sigma.add(*mzz);

RooConstVar* a1_p0_0_2nd[11] ;
RooConstVar* a2_p0_0_2nd[11] ;
RooConstVar* mean_p0_0_2nd[11] ;
RooConstVar* n1_p0_0_2nd[11] ;
RooConstVar* n2_p0_0_2nd[11] ;
RooConstVar* sigma_p0_0_2nd[11] ;


for(int i =0; i<11;i++){
  a1_p0_0_2nd[i]= new RooConstVar(Form("%s_%d_a1_p0_0_2nd",jetName[jetType].Data(),i),Form("%s_%d_a1_p0_0_2nd",jetName[jetType].Data(),i),dcbPara_2nd[0][i]);
  a2_p0_0_2nd[i]= new RooConstVar(Form("%s_%d_a2_p0_0_2nd",jetName[jetType].Data(),i),Form("%s_%d_a2_p0_0_2nd",jetName[jetType].Data(),i),dcbPara_2nd[1][i]);
  mean_p0_0_2nd[i]= new RooConstVar(Form("%s_%d_mean_p0_0_2nd",jetName[jetType].Data(),i),Form("%s_%d_mean_p0_0_2nd",jetName[jetType].Data(),i),dcbPara_2nd[2][i]);
  n1_p0_0_2nd[i]= new RooConstVar(Form("%s_%d_n1_p0_0_2nd",jetName[jetType].Data(),i),Form("%s_%d_n1_p0_0_2nd",jetName[jetType].Data(),i),dcbPara_2nd[3][i]);
  n2_p0_0_2nd[i]= new RooConstVar(Form("%s_%d_n2_p0_0_2nd",jetName[jetType].Data(),i),Form("%s_%d_n2_p0_0_2nd",jetName[jetType].Data(),i),dcbPara_2nd[4][i]);
  sigma_p0_0_2nd[i]= new RooConstVar(Form("%s_%d_sigma_p0_0_2nd",jetName[jetType].Data(),i),Form("%s_%d_sigma_p0_0_2nd",jetName[jetType].Data(),i),dcbPara_2nd[5][i]);

  formuList_a1.add(*a1_p0_0_2nd[i]);
  formuList_a2.add(*a2_p0_0_2nd[i]);
  formuList_mean.add(*mean_p0_0_2nd[i]);
  formuList_n1.add(*n1_p0_0_2nd[i]);
  formuList_n2.add(*n2_p0_0_2nd[i]);
  formuList_sigma.add(*sigma_p0_0_2nd[i]);


}//for(int i =0; i<11;i++)

RooFormulaVar* a1_p0_2nd= new RooFormulaVar("a1_p0_2nd"+jetName[jetType],"a1_p0_2nd"+jetName[jetType],formu_2nd,formuList_a1);
RooFormulaVar* a2_p0_2nd= new RooFormulaVar("a2_p0_2nd"+jetName[jetType],"a2_p0_2nd"+jetName[jetType],formu_2nd,formuList_a2);
RooFormulaVar* mean_p0_2nd= new RooFormulaVar("mean_p0_2nd"+jetName[jetType],"mean_p0_2nd"+jetName[jetType],formu_2nd,formuList_mean);
RooFormulaVar* n1_p0_2nd= new RooFormulaVar("n1_p0_2nd"+jetName[jetType],"n1_p0_2nd"+jetName[jetType],formu_2nd,formuList_n1);
RooFormulaVar* n2_p0_2nd= new RooFormulaVar("n2_p0_2nd"+jetName[jetType],"n2_p0_2nd"+jetName[jetType],formu_2nd,formuList_n2);
RooFormulaVar* sigma_p0_2nd= new RooFormulaVar("sigma_p0_2nd"+jetName[jetType],"sigma_p0_2nd"+jetName[jetType],formu_2nd,formuList_sigma);
  
RooFormulaVar *sigma_p0_up = new RooFormulaVar("sigma_p0_up"+jetName[jetType],"","@0+0.1*@0",*sigma_p0_2nd);
RooFormulaVar *sigma_p0_dn = new RooFormulaVar("sigma_p0_dn"+jetName[jetType],"","@0-0.1*@0",*sigma_p0_2nd);

RooDoubleCB dcrReso_piece("dcrReso"+jetName[jetType],"Double Crystal ball ",*mreco,*mzz,*mean_p0_2nd,*sigma_p0_2nd,*a1_p0_2nd,*n1_p0_2nd,*a2_p0_2nd,*n2_p0_2nd);
RooDoubleCB dcrReso_piece_up("dcrReso"+jetName[jetType]+"_up","dcb up",*mreco,*mzz,*mean_p0_2nd,*sigma_p0_up,*a1_p0_2nd,*n1_p0_2nd,*a2_p0_2nd,*n2_p0_2nd);
RooDoubleCB dcrReso_piece_dn("dcrReso"+jetName[jetType]+"_dn","dcb dn",*mreco,*mzz,*mean_p0_2nd,*sigma_p0_dn,*a1_p0_2nd,*n1_p0_2nd,*a2_p0_2nd,*n2_p0_2nd);
dcrReso_piece.plotOn(frame_mzz);
frame_mzz->Draw();
dcrReso_piece.Print("v");
*/



//?2nd Method:Resolutio parametrizatio: p0 stand for?
  RooRealVar *mean_p0_2nd = new RooRealVar("mean_p0_2nd","",0,-500,500);
  RooRealVar *sigma_p0_2nd = new RooRealVar("sigma_p0_2nd","",0,-500,500);
  RooRealVar *n1_p0_2nd = new RooRealVar("n1_p0_2nd","",0,-500,500);
  RooRealVar *n2_p0_2nd = new RooRealVar("n2_p0_2nd","",0,-500,500);
  RooRealVar *a1_p0_2nd = new RooRealVar("a1_p0_2nd","",0,-500,500);
  RooRealVar *a2_p0_2nd = new RooRealVar("a2_p0_2nd","",0,-500,500);//?not too big like, -500<a2_p0_2nd<500
  
  RooFormulaVar *sigma_p0_up = new RooFormulaVar("sigma_p0_up"+jetName[jetType],"","@0+0.1*@0",*sigma_p0_2nd);
  RooFormulaVar *sigma_p0_dn = new RooFormulaVar("sigma_p0_dn"+jetName[jetType],"","@0-0.1*@0",*sigma_p0_2nd);
  
  //It works without HZZ2L2QRooPdfs.h" (HZZ2L2QRooPdfs.h" may needs for "RooDoubleCB)"
  RooDoubleCB dcrReso_piece("dcrReso"+jetName[jetType],"Double Crystal ball ",*mreco,*mzz,*mean_p0_2nd,*sigma_p0_2nd,*a1_p0_2nd,*n1_p0_2nd,*a2_p0_2nd,*n2_p0_2nd);
  RooDoubleCB dcrReso_piece_up("dcrReso"+jetName[jetType]+"_up","dcb up",*mreco,*mzz,*mean_p0_2nd,*sigma_p0_up,*a1_p0_2nd,*n1_p0_2nd,*a2_p0_2nd,*n2_p0_2nd);
  RooDoubleCB dcrReso_piece_dn("dcrReso"+jetName[jetType]+"_dn","dcb dn",*mreco,*mzz,*mean_p0_2nd,*sigma_p0_dn,*a1_p0_2nd,*n1_p0_2nd,*a2_p0_2nd,*n2_p0_2nd);

//?Resolution input,dcb 6 parameters:2l2q_resolution_merged.root, 2l2q_resolution_resolved.root
  TFile *resoFile = new TFile("Input/2l2q_resolution_"+jetNameLower[jetType]+".root","read");
  TGraph *a1_gr = (TGraph*)resoFile->Get("a1_PN");//input for a1_p0_2nd->dcrReso_piece->reso->conv_template_1
  TGraph *a2_gr = (TGraph*)resoFile->Get("a2_PN"); 
  TGraph *n1_gr = (TGraph*)resoFile->Get("n1_PN"); 
  TGraph *n2_gr = (TGraph*)resoFile->Get("n2_PN"); 
  TGraph *mean_gr = (TGraph*)resoFile->Get("mean_PN"); 
  TGraph *sigma_gr = (TGraph*)resoFile->Get("sigma_PN"); 

 //Test for Resolutio inptu with Jilian data
//  TFile *resoFile = new TFile("2l2q_resolution_"+jetNameLower[jetType]+"_2016.root","read");
//  TGraphErrors *a1_gr = (TGraphErrors*)resoFile->Get("a1");//input for a1_p0_2nd->dcrReso_piece->reso->conv_template_1
//  TGraphErrors *a2_gr = (TGraphErrors*)resoFile->Get("a2"); 
//  TGraphErrors *n1_gr = (TGraphErrors*)resoFile->Get("n1"); 
//  TGraphErrors *n2_gr = (TGraphErrors*)resoFile->Get("n2"); 
//  TGraphErrors *mean_gr = (TGraphErrors*)resoFile->Get("mean"); 
//  TGraphErrors *sigma_gr = (TGraphErrors*)resoFile->Get("sigma"); 

  mzz->setVal(mass_d);//?perche mass_d(e' un valore solo,450,che puo' sostituire RooRealVar* mzz  
/*
  //Shoud be removed, For 2nd method resolution draw test
  for(int m=0;m<nbins;m++){
    double genval = low+(high-low)/double(nbins)*m; 
//    double genval = (low+(high-low)/double(nbins)*m)+100;//for test 
//    cout<<"genval="<<genval<<endl;//450,452,454,....3958: Mass scan range
    mzz->setVal(genval);
    a1_p0_2nd->setVal(a1_gr->Eval(genval));
    a2_p0_2nd->setVal(a2_gr->Eval(genval));
    n1_p0_2nd->setVal(n1_gr->Eval(genval));
    n2_p0_2nd->setVal(n2_gr->Eval(genval));
    mean_p0_2nd->setVal(mean_gr->Eval(genval));
    sigma_p0_2nd->setVal(sigma_gr->Eval(genval));
    int low_bound = genval + mean_p0_2nd->getVal()-sigma_p0_2nd->getVal()*30 ; 
    int high_bound = genval + mean_p0_2nd->getVal()+sigma_p0_2nd->getVal()*30 ; 
    if(low_bound<low_reco)
      low_bound=low_reco;
    if(high_bound>high_reco)
      high_bound=high_reco;

   
    for(int k = low_bound;k<high_bound;k++){
//      for(int j =0;j< template_1r->GetNbinsY(); j++){
        //conv_template_1->Fill(recoval, dbkg_val,template_1r->GetBinContent(m+1,j+1)*reso);
//      }//for(int j =0;j< template_1r->GetNbinsY(); j++)
    }//for(int k = low_bound;k<high_bound;k++)
  }//for(int m=0;m<nbins;m++)

dcrReso_piece.plotOn(frame_mzz);
frame_mzz->Draw();
dcrReso_piece.Print("v");
*/

//? Dbkg(zzjj) 2D templete signla input(2type?):"template_2l2q_merged_ggH_Moriond.root","template_2l2q_merged_ggH_Moriond.root"
//  TFile *ftemplate=new TFile(Form("Input/template_2l2q_%s_%s_Moriond.root",jetName[jetType].Data(),sampleName[sample].Data()));//cjlst rereco MC
//  TH2F *template_1= (TH2F*)ftemplate->Get("h2_KD_Zjj");//cjlst,start only with this
  TFile *ftemplate=new TFile("Input/2l2q_spin0_template2D_2016.root");//xBF UL
  TH2F *template_1= (TH2F*)ftemplate->Get("sig_resolved");//xBF,start only with this
//  TH2F *template_2= (TH2F*)ftemplate->Get("T_2D_2c");//bke
//  TH2F *template_124= (TH2F*)ftemplate->Get("T_2D_124c");//inteference
// Test for template_1


  TH2F *template_1r= new TH2F("template1","",nbins,low,high,30,0.,1.);//cont1*fa_sig,start only with this
//  TH2F *template_2r= new TH2F("template2","",nbins,low,high,30,0.,1.);//cont2*fa_bkg+cont1*fa_125+cont124*inter_125_bkg 
//  TH2F *template_124r= new TH2F("template124","",nbins,low,high,30,0.,1.);//cont124*inter_sig_bkg+cont1*inter_sig_125
  
//  TFile *ftemplate_dbkgup=new TFile(Form("/afs/test/template_2l2q_%s_up_%s_Moriond.root",jetName[jetType].Data(),sampleName[sample].Data()));//Not correct file
//  TH2F *template_1_dbkgup= (TH2F*)ftemplate->Get("T_2D_1c");
//  TH2F *template_2_dbkgup= (TH2F*)ftemplate->Get("T_2D_2c");
//  TH2F *template_124_dbkgup= (TH2F*)ftemplate->Get("T_2D_124c");

//  TH2F *template_1r_dbkgup= new TH2F("template1_dbkgup","",nbins,low,high,30,0.,1.);//start only with this 
//  TH2F *template_2r_dbkgup= new TH2F("template2_dbkgup","",nbins,low,high,30,0.,1.); 
//  TH2F *template_124r_dbkgup= new TH2F("template124_dbkgup","",nbins,low,high,30,0.,1.);
   
//  TFile *ftemplate_dbkgdn=new TFile(Form("/test/template_2l2q_%s_dn_%s_Moriond.root",jetName[jetType].Data(),sampleName[sample].Data()));//Not correct file,start only with this
//  TH2F *template_1_dbkgdn= (TH2F*)ftemplate->Get("T_2D_1c");
//  TH2F *template_2_dbkgdn= (TH2F*)ftemplate->Get("T_2D_2c");
//  TH2F *template_124_dbkgdn= (TH2F*)ftemplate->Get("T_2D_124c");

//  TH2F *template_1r_dbkgdn= new TH2F("template1_dbkgdn","",nbins,low,high,30,0.,1.); //start only with this
//  TH2F *template_2r_dbkgdn= new TH2F("template2_dbkgdn","",nbins,low,high,30,0.,1.); 
//  TH2F *template_124r_dbkgdn= new TH2F("template124_dbkgdn","",nbins,low,high,30,0.,1.); 

  TH2F *conv_template_1= new TH2F("conv_template1","",nbins_reco/10,low_reco,high_reco,30,0.,1.);//start only with this 
//  TH2F *conv_template_2= new TH2F("conv_template2","",nbins_reco/10,low_reco,high_reco,30,0.,1.); 
//  TH2F *conv_template_124= new TH2F("conv_template124","",nbins_reco/10,low_reco,high_reco,30,0.,1.); 
  
  TH2F *conv_template_1_up= new TH2F("conv_template1_up","",nbins_reco/10,low_reco,high_reco,30,0.,1.); //start only with this
//  TH2F *conv_template_2_up= new TH2F("conv_template2_up","",nbins_reco/10,low_reco,high_reco,30,0.,1.); 
//  TH2F *conv_template_124_up= new TH2F("conv_template124_up","",nbins_reco/10,low_reco,high_reco,30,0.,1.); 

  TH2F *conv_template_1_dn= new TH2F("conv_template1_dn","",nbins_reco/10,low_reco,high_reco,30,0.,1.);//start only with this 
//  TH2F *conv_template_2_dn= new TH2F("conv_template2_dn","",nbins_reco/10,low_reco,high_reco,30,0.,1.); 
//  TH2F *conv_template_124_dn= new TH2F("conv_template124_dn","",nbins_reco/10,low_reco,high_reco,30,0.,1.); 

//  TH2F *conv_template_1_dbkgup= new TH2F("conv_template1_dbkgup","",nbins_reco/10,low_reco,high_reco,30,0.,1.); 
//  TH2F *conv_template_2_dbkgup= new TH2F("conv_template2_dbkgup","",nbins_reco/10,low_reco,high_reco,30,0.,1.); 
//  TH2F *conv_template_124_dbkgup= new TH2F("conv_template124_dbkgup","",nbins_reco/10,low_reco,high_reco,30,0.,1.); 
  
//  TH2F *conv_template_1_dbkgdn= new TH2F("conv_template1_dbkgdn","",nbins_reco/10,low_reco,high_reco,30,0.,1.);//start only with this 
//  TH2F *conv_template_2_dbkgdn= new TH2F("conv_template2_dbkgdn","",nbins_reco/10,low_reco,high_reco,30,0.,1.); 
//  TH2F *conv_template_124_dbkgdn= new TH2F("conv_template124_dbkgdn","",nbins_reco/10,low_reco,high_reco,30,0.,1.); 

//?for what? "template_merge"
  TH2F *template_merge= new TH2F("template_merge","",nbins_reco,low_reco,high_reco,1,0.,1.); 

//?Input for Interfer1ence or Line shape:Da Axel
  TString phaseName[2]={"fphase_ggH.root","fgraph_vbf_phase.root"};
  TString cName[2]={"cosspline","cos"};
  TString sName[2]={"sinspline","sin"};
  TFile *fphase_noweight=new TFile("Input/"+phaseName[sample]);
  TGraph *cosfunc = (TGraph*)fphase_noweight->Get(cName[sample]);
  TGraph *sinfunc = (TGraph*)fphase_noweight->Get(sName[sample]);

/*
//?Input of Efficiency of Signal(sample=0=ggH,1=VBFH)+ 3 cathegories: btag,vbftag,untag
//efficiency_2l2q_spin0_ggH.root, efficiency_2l2q_spin0_ggH.root
  TFile *sigeff = new TFile ("Input/eff_sig.root");//Not correct sample
  TGraph *effsig=((TGraph*)(sigeff->Get("effsig")));	
  TGraph *effsig_btag=((TGraph*)(sigeff->Get("effsig_btag")));
  TGraph *effsig_vbftag=((TGraph*)(sigeff->Get("effsig_vbftag")));
  TGraph *effsig_untag=((TGraph*)(sigeff->Get("effsig_untag")));
*/

//xBF input test  
  TFile *sigeff = new TFile ("Input/2l2q_Efficiency_spin0_ggH_2016.root");//Not correct sample
//  TGraph *effsig=((TGraph*)(sigeff->Get("effsig")));	
  TGraphErrors *effsig_btag=((TGraphErrors*)(sigeff->Get("spin0_ggH_eeqq_Merged_b-tagged")));
  TGraphErrors *effsig_vbftag=((TGraphErrors*)(sigeff->Get("spin0_ggH_eeqq_Merged_vbf-tagged")));
  TGraphErrors *effsig_untag=((TGraphErrors*)(sigeff->Get("spin0_ggH_eeqq_Merged_untagged")));
  cout<<"spin0_"<<sampleName[sample]<<"_"<<"chan:"<<chan<<"_"<<"jetType:"<<jetType<<"_untagged"<<endl;

//?Input of Efficiency of Backgroud: which background? DY,TT,WW,WZ?
//  TFile *bkgeff = new TFile ("Input/eff_vbf_bkg.root");
//  TGraph *effbkg = (TGraph*)(bkgeff->Get("eff_"+chanName[chan]+"_"+jetName[jetType]+"_"+cateName[cate])); 
//  TGraph *effbkg_btag=((TGraph*)(bkgeff->Get("eff_"+chanName[chan]+"_"+jetName[jetType]+"_b-tagged")));
//  TGraph *effbkg_vbftag=((TGraph*)(bkgeff->Get("eff_"+chanName[chan]+"_"+jetName[jetType]+"_vbf-tagged")));
//  TGraph *effbkg_untag=((TGraph*)(bkgeff->Get("eff_"+chanName[chan]+"_"+jetName[jetType]+"_untagged")))

//:wq
//:? Nomalization
  float ratio_btag=0 ;
  float ratio_vbftag = 0;
//  cout<<"effsig_untag(1050)=" <<effsig_untag->Eval(1050)<<endl;//cjlst:0.261047, xBF(ggH2e2q):0.13225
//  cout<<"effsig_untag("<<mass_d<<")=" <<effsig_untag->Eval(mass_d)<<endl;//cjlst:0.261047 ,xBF(ggH2e2q):0.13225
//  cout<<"effsig_btag("<<mass_d<<")=" <<effsig_btag->Eval(mass_d)<<endl;//cjlst:0.183752 ,xBF(ggH2e2q):0.474476
//  cout<<"effsig_vbftag("<<mass_d<<")=" <<effsig_vbftag->Eval(mass_d)<<endl;//cjlst:0.249157 ,xBF(ggH2e2q):0.0102876
  if(effsig_untag->Eval(mass_d)!=0)
    ratio_btag=-1*(effsig_btag->Eval(mass_d)/effsig_untag->Eval(mass_d));
    cout<<"ratio_btag="<<ratio_btag<<endl;//?why minus value? cjlst:-0.703902
  if((effsig_untag->Eval(mass_d)+effsig_btag->Eval(mass_d))!=0)
    ratio_vbftag=-1*(effsig_vbftag->Eval(mass_d)/(effsig_untag->Eval(mass_d)+effsig_btag->Eval(mass_d)));
    cout<<"ratio_vbftag="<<ratio_vbftag<<endl;//?why minus value? cjlst:-0.560157
  if(cate==1) ratio_btag = 1;//cate=1:b-tagged
  if(cate==0) ratio_vbftag = 1;//cate=0:vbf-tagged

  cout << "Gen "<<sigfull<<endl;//3.36826 

  TAxis *xax = template_1->GetXaxis();
//  cout<<"HERE:*xax = template_1->GetXaxis()"<<endl;

//for (int i =0; i< template_1->GetNbinsX();i++){
  for (int i =0; i< nbins;i++){

//!x is Mvv scan range
//    cout<<"nbins="<<nbins<<endl;//n=1575
    double x = low+ (high-low)/double(nbins)*i;//453,455,457,...3599. 
//    cout<<"x="<<x<<endl;//450,452,454,...3598.
//    double x = (high-low)/double(nbins)*i;//For test:x=0,2,4,...3149 
    mzz->setVal(x);//RooRealVar* mzz value setting as x
    double bc =sigfull*(fabs((mass_d-x))<0.1);//sigfull=3.36826 ?bc:what is this value
    cout<<"bc="<<bc<<endl;//0, 0, 0, 0, 0....0.
    if(width_d>0.5)
    bc = pdf->getVal(*mzz)*sigfull*bwid;//?0.0.625232, 0.611569,0.598418....9.78313e-08,9.7315e-08
//    cout<<"bc(if width_d>0.5)="<<bc<<endl;//
//    double bc_bkg= genm->GetBinContent(i+1);//?for Interferance?(ggzz, vbfbkg related value)
//    cout<<"bc_bkg="<<bc_bkg<<endl;//-nan,-nan,....-nan,?:genm is empty histo.
//    double bc_125= pdf125_hist->GetBinContent(i+1);//!Not tested ude to pdf125    
//    cout<<"bc_bkg="<<bc_bkg<<endl;//"Kfactor_Collected_ggHZZ_2l2l_NNLO_NNPDF_NarrowWidth_13TeV.root"
//    double effval=effsig->Eval(x);//Original, effsig not available
    double effval=(effsig_btag->Eval(x))+(effsig_vbftag->Eval(x))+(effsig_untag->Eval(x));//Just for Test
//    cout<<"effval="<<effval<<endl;

//    double effbkg_val=effbkg->Eval(x);//"eff_vbf_bkg.root" need
//    double kfac=ggzz_kf->Eval(x);

//Important
    double fa_sig=bc*effval;//0.00475365, 0.000571186,0.000662665......2.541741e-08
//    cout<<"fa_sig=bc*effval: "<<fa_sig<<endl;
//    double fa_bkg=bc_bkg*effval;//?bc_bkg*effbkg_val,No
//    double fa_125=bc_125*effval;//No,
    
    double a= (x*x-mean->getVal()*mean->getVal());//Mvv*Mvv-Mh*Mh: 186875,188679, 190491,......1.29156e+07,1.293e+07
    double b = mean->getVal()*sigma->getVal();//Mh*Th(Gammma_h=Width of h); 0.50875, 0.50875,.....0.50875
//    cout<<"a="<<a<<endl;//0,.....
//    cout<<"b="<<b<<endl;//

    double cossig = a/TMath::Sqrt(a*a+b*b);//1,1,1.......1.
    double sinsig = b/TMath::Sqrt(a*a+b*b);//2.67073e-6,2.69638e-6,.....3.93465e-8.
//    cout<<"cossig = a/TMath::Sqrt(a*a+b*b)="<<cossig<<endl;
//    cout<<"sinsig = b/TMath::Sqrt(a*a+b*b)="<<sinsig<<endl;

    double a125= (x*x-125.*125.);//!same as a:186875,188679, 190491,......1.29156e+07,1.293e+07
    double b125 = 125*0.00407;// !same as b:0.50875, 0.50875,.....0.50875

    double cossig125 = a125/TMath::Sqrt(a125*a125+b125*b125);//1,1,1,.......1.
    double sinsig125 = b125/TMath::Sqrt(a125*a125+b125*b125);//2.5718e-6,2.54799e-06,2.52443e-6,....3.93465e-08
//    cout<<"cossig125 = a125/TMath::Sqrt(a125*a125+b125*b125)="<<cossig<<endl;//0,0.0853475,...0.999999.
//    cout<<"sinsig125 = b125/TMath::Sqrt(a125*a125+b125*b125)="<<sinsig<<endl;//1,0.996351,...0.00165266.
    
    double maphase=x;
    if(x>1600){
      if(sample==0)
        maphase=1600.;
	else if(sample==1&&x>3000)
	maphase=3000.;
    }//if(x>1600)
    double cosfuncv=cosfunc->Eval(maphase);//-0.427367,-0.429459,-0.431552...-0.836762
    double sinfuncv=sinfunc->Eval(maphase);//0.056392,0.0539416,0.0514912,...0.128603
//    cout<<"cosfuncv="<<cosfuncv<<endl;
//    cout<<"sinfuncv="<<sinfuncv<<endl;

    if(sample){
      cosfuncv = cosfuncv/1.76*2;//?for vbfH?what are 1.76,2:-0.485644,-0.488022,-0.4904,...-0.950866 
      sinfuncv = sinfuncv/1.76*2;//?for vbfH?what are 1.76,2:-0.0640818,0.0612972,0.0585127,...0.14614
//    cout<<"cosfuncv="<<cosfuncv<<endl;
//    cout<<"sinfuncv="<<sinfuncv<<endl;
    }//if(sample)
		
//    double sigbkgsqrt = TMath::Sqrt(fa_sig*fa_bkg); 
//    double hbkgsqrt = TMath::Sqrt(fa_125*fa_bkg); 
//    double sighsqrt = TMath::Sqrt(fa_sig*fa_125); 

//    double inter_sig_bkg = 1.76*sigbkgsqrt*(cosfuncv*cossig+sinfuncv*sinsig);
//    double inter_125_bkg = 1.76*hbkgsqrt* (cosfuncv*cossig125+sinfuncv*sinsig125); 
//    double inter_sig_125 = 2*sighsqrt * (cossig125*cossig-sinsig125*sinsig); 

    int binN = xax->FindBin(x);//7,7,7,7,7,7,7,8,8,8,8........90,90,90
//    cout<<"binN="<<binN<<endl;

//   for(int j=0;j<template_1->GetNBinsY();j++){
    for(int j=0;j<10;j++){
     double cont1, cont2, cont124;
     cont1 = template_1->GetBinContent(binN,j+1);//Not Zero.
     cout<<"cont1(template1)=" <<cont1<<endl;
//     cont1_dbkgup = template_1_dbkgup->GetBinContent(binN,j+1);
//     cont1_dbkgdn = template_1_dbkgdn->GetBinContent(binN,j+1);
     template_1r->SetBinContent(i+1,j+1,cont1*fa_sig);//very small value....=2.95901e-10
     cout<<"template_1r:cont1*fa_sig="<<cont1*fa_sig<<endl;
//     template_1r_dbkgup->SetBinContent(i+1,j+1,cont1_dbkgup*fa_sig);
//     template_1r_dbkgdn->SetBinContent(i+1,j+1,cont1_dbkgdn*fa_sig);

    }//for(int j=0;j<template_1->GetNBinsY();j++)
  }//for (int i =0; i< nbins;i++)

  cout << "Gen sig= "<<template_1r->Integral()<<endl;//xBF:93.4356, cjlst:3114.8

  //Signal input
  TH1F *template_1_proj = (TH1F*)template_1r->ProjectionX();
//  template_1->Draw("Colz");
//  template_1r->Draw("Colz");
//  template_1_proj->Draw("histo");

  RooDataHist *pdfsig_hist_hist_gen= new RooDataHist("pdfsig_hist_hist_gen"+jetName[jetType]+chanName[chan]+cateName[cate]+sampleName[sample],"",RooArgSet(*mzz),template_1_proj);
  RooHistFunc *pdfsig_hist_func_gen= new RooHistFunc("pdfsig_hist_func_gen"+jetName[jetType]+chanName[chan]+cateName[cate]+sampleName[sample],"",RooArgSet(*mzz),*pdfsig_hist_hist_gen);
//Final model=>should be modified for only using signla pdf.
//  HZZ4L_RooHighmass_1D *bsi_hist_gen=new HZZ4L_RooHighmass_1D("bsi_hist_gen","bsi_hist_gen",*mzz,*signorm,RooArgList(*pdfsig_hist_func_gen,*hgenm_func_gen,*inter_func_gen));//signal+interferece+SM125 
  HZZ4L_RooHighmass_1D *bsi_hist_gen=new HZZ4L_RooHighmass_1D("bsi_hist_gen","bsi_hist_gen",*mzz,*signorm,RooArgList(*pdfsig_hist_func_gen));//Singl only model 
//  bsi_hist_gen->plotOn(frame_mzz);//warning: null passed to a calle
//  frame_mzz->Draw();
  pdfsig_hist_hist_gen->Print("v");
  pdfsig_hist_func_gen->Print("v");

 
//  for(int m=0;m<nbins;m++){
  for(int m=1;m<2;m++){//Jsut for Test
    double genval = low+(high-low)/double(nbins)*m;////450,452,454,....3958: Mass scan range 
    mzz->setVal(genval);
    a1_p0_2nd->setVal(a1_gr->Eval(genval));
    a2_p0_2nd->setVal(a2_gr->Eval(genval));
    n1_p0_2nd->setVal(n1_gr->Eval(genval));
    n2_p0_2nd->setVal(n2_gr->Eval(genval));
    mean_p0_2nd->setVal(mean_gr->Eval(genval));
    sigma_p0_2nd->setVal(sigma_gr->Eval(genval));

    int low_bound = genval + mean_p0_2nd->getVal()-sigma_p0_2nd->getVal()*30 ;//291,291, 291,...,2894 
    int high_bound = genval + mean_p0_2nd->getVal()+sigma_p0_2nd->getVal()*30 ;//1186,1190,1195,...,4320 
//    cout<<"low_bound="<<low_bound<<endl;
//    cout<<"genval="<<genval<<endl;//450,452,454,....3958: Mass scan range
//    cout<<"high_bound="<<high_bound<<endl;

    if(low_bound<low_reco)
      low_bound=low_reco;
    if(high_bound>high_reco)
      high_bound=high_reco;

   
    for(int k = low_bound;k<high_bound;k++){
      double recoval = k;//500,501....1186
      mreco->setVal(recoval);//0x36796c0
      double reso = dcrReso_piece.getVal(*mreco); //0.0295321, 0.0271645,...0.000145856
//      cout<<"recoval="<<recoval<<endl;
//      cout<<"mreco="<<mreco<<endl;
//      cout<<"reso="<<reso<<endl;
      if(reso<1.E-5)
        continue;
//      double reso_up = dcrReso_piece_up.getVal(*mreco); 
//      double reso_dn = dcrReso_piece_dn.getVal(*mreco);
      for(int j =0;j< template_1r->GetNbinsY(); j++){
        double dbkg_val = template_1r->GetYaxis()->GetBinCenter(j+1);
        cout<< recoval<<"\t"<<genval<<"\t"<<template_1->GetBinContent(m+1,j+1)<<"\t"<<reso<<endl;//500	452	777.965	0.0295321
        conv_template_1->Fill(recoval, dbkg_val,template_1r->GetBinContent(m+1,j+1)*reso);

      }//for(int j =0;j< template_1r->GetNbinsY(); j++)
    }//for(int k = low_bound;k<high_bound;k++)
  }//for(int m=0;m<nbins;m++)
  cout << "reco sig "<< conv_template_1->Integral()<<endl;// cjlst:14.0828, xBF:0.732724
//  cout << "reco bkg "<< conv_template_2->Integral()<<endl;
//  conv_template_1->Draw("colz");
  TH1F *conv_template_1_proj = (TH1F*)conv_template_1->ProjectionX();
  conv_template_1_proj->Draw("histo"); 

  //cout << "reco sig "<< conv_template_1->Integral()<<endl;
  if(is2D){
    //signal only
    RooDataHist *pdfsig_hist_hist= new RooDataHist("pdfsig_hist_hist"+jetName[jetType]+chanName[chan]+cateName[cate]+sampleName[sample],"",RooArgSet(*mreco,*dbkg),conv_template_1);
    RooHistFunc *pdfsig_hist_func= new RooHistFunc("pdfsig_hist_func"+jetName[jetType]+chanName[chan]+cateName[cate]+sampleName[sample],"",RooArgSet(*mreco,*dbkg),*pdfsig_hist_hist);
    //final model without bkg,interference
    HZZ4L_RooHighmass *bsi_hist=new HZZ4L_RooHighmass("bsi_hist","bsi_hist",*mreco,*dbkg,*signorm,RooArgList(*pdfsig_hist_func)); 
//    bsi_hist->SetNameTitle(dataName[sample],dataName[sample]);

   //work space 
    w.import(*bsi_hist,RecycleConflictNodes());
  }//if(is2D)
  else{
//    RooDataHist *pdfsig_hist_hist= new RooDataHist("pdfsig_hist_hist"+jetName[jetType]+chanName[chan]+cateName[cate]+sampleName[sample],"",RooArgSet(*mreco),conv_template_1_proj);
//    RooHistFunc *pdfsig_hist_func= new RooHistFunc("pdfsig_hist_func"+jetName[jetType]+chanName[chan]+cateName[cate]+sampleName[sample],"",RooArgSet(*mreco),*pdfsig_hist_hist);
//    HZZ4L_RooHighmass_1D *bsi_hist=new HZZ4L_RooHighmass_1D("bsi_hist","bsi_hist",*mreco,*signorm,RooArgList(*pdfsig_hist_func,*hgenm_func,*inter_func)); 
//    bsi_hist->SetNameTitle(dataName[sample],dataName[sample]);
//    w.import(*bsi_hist,RecycleConflictNodes());
  }//else

  cout << "Reco Sig "<<conv_template_1_proj->Integral()<<endl;//xBF:0.732724,
  ProcessNormalization* int_sig;
  float int_sig_norm = conv_template_1->Integral();

  cout<<" int_sig_norm=" <<int_sig_norm<<endl;//xBF:0.732724
//  cout<< int_int_norm <<"\t"<<int_int_dn<<"\t"<<int_int_up<<endl;

  int_sig = new ProcessNormalization("int_sig"+jetName[jetType]+chanName[chan]+cateName[cate]+sampleName[sample],"int_sig"+jetName[jetType]+chanName[chan]+cateName[cate]+sampleName[sample],int_sig_norm);

  RooRealVar *sysrr=new RooRealVar("CMS_2l2q"+jetName[jetType]+"Res","2l2q"+jetName[jetType]+"Res",-7,7);
  RooRealVar *kfacrr=new RooRealVar("kfactor_ggZZ","kfactor_ggZZ",-7,7);
  RooRealVar *btag= new RooRealVar("BTAG_"+jetNameLower[jetType],"BTAG_"+jetNameLower[jetType],0,-3,3); 
  RooRealVar *jes= new RooRealVar("JES","JES",0,-3,3); 

  float errbtag = btagU[jetType]*ratio_btag;
  float errjes = jesU[sample]*ratio_vbftag;
  cout<<"errbtag="<< errbtag<<"\t"<<"errjes="<<errjes<<endl;

  string formula = Form("(@0*@3+@1+@2*sqrt(@3))*(@4*(%.3f)+1)*(1+@5*(%.3f))",errbtag, errjes);
//  RooFormulaVar *ggH_norm=new RooFormulaVar(dataName[sample]+"_norm",dataName[sample]+"_norm",formula.c_str(),RooArgList(*int_sig,*int_bkg,*int_int,*signorm,*btag,*jes));
//  w.import(*ggH_norm,RecycleConflictNodes());
//  cout << "final "<<ggH_norm->getVal()<<endl;


  TFile *fwork ;
    if(width_d<0.1)
      fwork= new TFile(Form("workspace_template%s_2l2q_Moriond/hzz4l_%d_%d_%d_%dS_13TeV.input_func_%2.0f_%2.2f.root",workName[is2D].Data(),chan,jetType,cate,sample,mass_d,width_d),"recreate");
    else
      fwork= new TFile(Form("workspace_template%s_2l2q_Moriond/hzz4l_%d_%d_%d_%dS_13TeV.input_func_%2.0f_%2.1f.root",workName[is2D].Data(),chan,jetType,cate,sample,mass_d,width_d),"recreate");
      fwork->cd();
      w.Write();
      fwork->Close();
//      w.Print();

cout<<"##### Good Job!#####"<<endl;
}//End of dosomething()

  void llqq_Run2Comb(double m=1050,double w=123.0,int cate=0, int is2D=1,double inteval=2,int loo=1){
  gROOT->ProcessLine(".x tdrstyle.cc");
    for(int i=0;i<loo;i++){
      dosomething(0,0,0,cate,is2D,m,w);
//      dosomething(1,0,0,cate,is2D,m,w);
//      dosomething(0,1,0,cate,is2D,m,w);
//      dosomething(1,1,0,cate,is2D,m,w);
//      dosomething(0,0,1,cate,is2D,m,w);
//      dosomething(1,0,1,cate,is2D,m,w);
//      dosomething(0,1,1,cate,is2D,m,w);
//      dosomething(1,1,1,cate,is2D,m,w);
//      dosomething(0,0,2,cate,is2D,m,w);
//      dosomething(1,0,2,cate,is2D,m,w);
//      dosomething(1,1,2,cate,is2D,m,w);
//      dosomething(0,1,2,cate,is2D,m,w);
      m +=inteval; 
    }//for(int i=0;i<loo;i++)  
  }//void llqq_Run2Comb



